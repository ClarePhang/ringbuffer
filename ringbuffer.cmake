
include(ExternalProject)

# project(ringbuffer)

# set(src_dir     "${CMAKE_CURRENT_LIST_DIR}/${PROJECT_NAME}")
set(src_dir     "${CMAKE_CURRENT_LIST_DIR}")

# set(fast_github_prefix "https://github.91chifun.workers.dev")
# set(dl          "${fast_github_prefix}/https://github.com/dennis-musk/ringbuffer/archive/refs/heads/master.zip")
# set(proj        "${PROJECT_NAME}_download")

# ExternalProject_Add(    "${proj}"
#     # DOWNLOAD_DIR        "!use default"
#     URL                 "${dl}"
#     SOURCE_DIR          "${src_dir}"
#     BINARY_DIR          ""
#     UPDATE_COMMAND      ""
#     CONFIGURE_COMMAND   ""
#     BUILD_COMMAND       ""
#     INSTALL_COMMAND     ""
#     TEST_COMMAND        ""
#     DEPENDS             ""
# )

message(STATUS ${src_dir})

# library
set(${PROJECT_NAME}_sources
    ${src_dir}/user/ringbuffer.c
)

add_library(${PROJECT_NAME} SHARED
    ${${PROJECT_NAME}_sources}
)

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(FILES ${src_dir}/user/ringbuffer.h DESTINATION include)

# test
if(BUILD_TESTING)
set(${PROJECT_NAME}_test_sources
    ${src_dir}/user/buffer_test.c
)

add_executable(${PROJECT_NAME}_test
    ${${PROJECT_NAME}_test_sources}
)

target_include_directories(${PROJECT_NAME}_test
    PRIVATE
    ${src_dir}/user
)

target_link_libraries(${PROJECT_NAME}_test
    ${PROJECT_NAME} pthread rt m
)

add_test(NAME ${PROJECT_NAME}_test
    COMMAND ${PROJECT_NAME}_test
)

install(TARGETS ${PROJECT_NAME}_test DESTINATION bin)
endif()
